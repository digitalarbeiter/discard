#!/usr/bin/env python3
# coding: utf-8
from flask import Flask, jsonify, request, session
import json
import os
import multiprocessing
import sys
import time
import contextlib
import uuid

import discard


app = Flask(__name__)


# Global GAMES list and lock. The idea is this: for each game_id both the game
# and if it's currently being modified. The GAME_LOCK is global for all
# individual (concurrent) games and used only while updating whether this
# particular game is being updated.
GAMES = {}
GAME_LOCK = multiprocessing.Lock()


@contextlib.contextmanager
def game_lock(game_id):
    """ Safeguard game against concurrent moves.

        @param game_id (str) Game UUID.
        @return Game object, exclusive to this context.
    """
    GAME_LOCK.acquire()
    try:
        game = GAMES[game_id][1]
        while GAMES[game_id][0]:
            time.sleep(0.01)
        GAMES[game_id] = (1, game)
    finally:
        GAME_LOCK.release()
    yield game
    GAMES[game_id] = (0, game)


def proper_stacktrace(f):
    def _executor(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except Exception as ex:
            print(f"{f.__name__}(*args={args}, **kwargs={kwargs}) --> {ex}", file=sys.stderr)
            game_id = kwargs.get("game_id")
            if game_id:
                print(f"{game_id}: {GAMES[game_id]}", file=sys.stderr)
            raise
    # fucking flask being a nanny and checking for unique @app.route-decorated
    # function __name__ m(
    _executor.__name__ = f.__name__
    return _executor


def game_list(player):
    """ Get list of games the player is involved with.

        @param player (str) The current player.
        @return Games dict game_id to dict with game_type and players.
    """
    games = {}
    GAME_LOCK.acquire()
    try:
        for game_id, (_, game) in GAMES.items():
            if player in game.hand.keys():
                games[game_id] = {
                    "game_id": game_id,
                    "game_type": game.game_type,
                    "players": list(game.hand.keys()),
                }
    finally:
        GAME_LOCK.release()
    return games


def ok(view=None, **kwargs):
    """ Signal OK over the API.

        @param view (dict) Current player's view of their current game (optional).
        @param kwargs (dict) Optional other entries in OK status.
        @return JSON-encoded Flask response.
    """
    response = {"status": "ok", "error": None, "view": view}
    response.update(**kwargs)
    return jsonify(response)


def error(msg, **kwargs):
    """ Signal error over the API.

        @param msg (str) Error message.
        @param kwargs (dict) Optional other entries in ERROR status.
        @return JSON-encoded Flask response.
    """
    response = {"status": "error", "error": msg}
    response.update(**kwargs)
    return jsonify(response)


@app.route("/game/<game_id>/shuffle")
@proper_stacktrace
def shuffle(game_id):
    """ Shuffle the deck.

        @param game_id (str) Game UUID.
    """
    player = session.get("player")
    if not player:
        return error("not-logged-in")
    with game_lock(game_id) as game:
        if player not in game.hand:
            return error("player-not-in-game {}".format(player))
        game.shuffle()
        return ok(game.view(player))


@app.route("/game/<game_id>/stack_to_deck")
@proper_stacktrace
def stack_to_deck(game_id):
    """ Put cards from stack to deck, and re-shuffle.

        @param game_id (str) Game UUID.
    """
    player = session.get("player")
    if not player:
        return error("not-logged-in")
    stack = get_stack()
    with game_lock(game_id) as game:
        if player not in game.hand:
            return error("player-not-in-game {}".format(player))
        game.stack_to_deck(stack)
        return ok(game.view(player))


@app.route("/game/<game_id>/deal")
@proper_stacktrace
def deal(game_id):
    """ Deal n cards to each player.

        @param game_id (str) Game UUID.
    """
    player = session.get("player")
    if not player:
        return error("not-logged-in")
    n = request.args.get("n", default=1, type=int)
    with game_lock(game_id) as game:
        if player not in game.hand:
            return error("player-not-in-game {}".format(player))
        game.deal(n)
        return ok(game.view(player))


@app.route("/game/<game_id>/view")
@proper_stacktrace
def view(game_id):
    """ View from the player.

        @param game_id (str) Game UUID.
    """
    player = session.get("player")
    if not player:
        return error("not-logged-in")
    with game_lock(game_id) as game:
        if player not in game.hand:
            return error("player-not-in-game {}".format(player))
        return ok(game.view(player))


@app.route("/game/<game_id>/draw")
@proper_stacktrace
def draw(game_id):
    """ Player draws one card from the deck.

        @param game_id (str) Game UUID.
    """
    player = session.get("player")
    if not player:
        return error("not-logged-in")
    with game_lock(game_id) as game:
        if player not in game.hand:
            return error("player-not-in-game {}".format(player))
        game.draw(player)
        return ok(game.view(player))


def get_stack():
    """ Get stack parameter (attn: None vs. null)

        @return Stack name (str).
    """
    stack = request.args.get("stack", default=None, type=str)
    return None if stack == "null" else stack


@app.route("/game/<game_id>/show")
@proper_stacktrace
def show(game_id):
    """ Take one cards from the deck and show it on the stack.

        @param game_id (str) Game UUID.
    """
    player = session.get("player")
    if not player:
        return error("not-logged-in")
    stack = get_stack()
    with game_lock(game_id) as game:
        if player not in game.hand:
            return error("player-not-in-game {}".format(player))
        game.show(stack)
        return ok(game.view(player))


@app.route("/game/<game_id>/draw_from_stack")
@proper_stacktrace
def draw_from_stack(game_id):
    """ Player takes top card (or all cards) from stack.

        @param game_id (str) Game UUID.
    """
    player = session.get("player")
    if not player:
        return error("not-logged-in")
    stack = get_stack()
    all = request.args.get("all", default=False, type=bool)
    with game_lock(game_id) as game:
        if player not in game.hand:
            return error("player-not-in-game {}".format(player))
        game.draw_from_stack(player, stack, all)
        return ok(game.view(player))


@app.route("/game/<game_id>/push")
@proper_stacktrace
def push(game_id):
    """ Player pushes card to their tricks.

        @param game_id (str) Game UUID.
    """
    player = session.get("player")
    if not player:
        return error("not-logged-in")
    card = request.args.get("card", type=str)
    with game_lock(game_id) as game:
        if player not in game.hand:
            return error("player-not-in-game {}".format(player))
        game.push(player, card)
        return ok(game.view(player))


@app.route("/game/<game_id>/play")
@proper_stacktrace
def play(game_id):
    """ Player places card on the stack.

        @param game_id (str) Game UUID.
    """
    player = session.get("player")
    if not player:
        return error("not-logged-in")
    stack = get_stack()
    card = request.args.get("card", type=str)
    with game_lock(game_id) as game:
        if player not in game.hand:
            return error("player-not-in-game {}".format(player))
        game.play(player, card, stack)
        return ok(game.view(player))


@app.route("/game/<game_id>/take_trick")
@proper_stacktrace
def take_trick(game_id):
    """ Player takes the trick (all cards on the stack).

        @param game_id (str) Game UUID.
    """
    player = session.get("player")
    if not player:
        return error("not-logged-in")
    stack = get_stack()
    with game_lock(game_id) as game:
        if player not in game.hand:
            return error("player-not-in-game {}".format(player))
        game.take_trick(player, stack)
        return ok(game.view(player))


@app.route("/new/<game_type>")
@proper_stacktrace
def new_game(game_type):
    player = session.get("player")
    if not player:
        return error("not-logged-in")
    players = request.args.getlist("players", type=str)
    if player not in players:
        return error("player-not-in-game {}".format(player))
    with open("accounts.json", "r") as f:
        passwords = json.load(f)
    for p in players:
        if p not in passwords:
            return error("unknown-player {}".format(p))
    with open("games.json", "r") as f:
        game_templates = json.load(f)
    if game_type not in game_templates:
        return error("unknown-game {}".format(game_type))
    template = game_templates[game_type]
    game = discard.Game(
        game_type,
        deck=template["deck"],
        players=players,
        tricks=template.get("tricks", False),
        auto_sort=template.get("auto_sort", False),
    )
    game.shuffle()
    n_cards = template.get("deal", {"default": 0})
    if "default" not in n_cards:
        n_cards["default"] = 0
    n = n_cards.get(str(len(players)), n_cards["default"])  # str() b/c json
    game.deal(n)
    game_id = str(uuid.uuid1())
    GAMES[game_id] = (0, game)
    return ok(game.view(player), game_id=game_id, players=players, game_type=game_type)


@app.route("/games")
@proper_stacktrace
def games():
    """ List of games the player is involved in.
    """
    player = session.get("player")
    if not player:
        return error("not-logged-in")
    return ok(None, games=game_list(player))


@app.route("/players")
@proper_stacktrace
def players():
    """ List of all players.
    """
    player = session.get("player")
    if not player:
        return error("not-logged-in")
    with open("accounts.json", "r") as f:
        passwords = json.load(f)
    players = [
        player
        for player in passwords.keys()
        if player not in ["root", "admin", "", None, "null"]
    ]
    return ok(None, players=players)


@app.route("/login")
@proper_stacktrace
def login():
    """ Authenticate user/player.
    """
    auth = request.authorization
    user = auth.username
    password = auth.password
    with open("accounts.json", "r") as f:
        passwords = json.load(f)
    with open("games.json", "r") as f:
        game_types = json.load(f)
        game_types = [(gt, g["description"]) for gt, g in game_types.items()]
    if passwords.get(user) == (password or "no-password-provided"):
        session["player"] = user
        players = [
            player
            for player in passwords.keys()
            if player not in ["root", "admin", "", None, "null"]
        ]
        return ok(None, games=game_list(user), game_types=game_types, players=players)
    else:
        return error("invalid-password {}".format(user))


@app.route("/logout")
@proper_stacktrace
def logout():
    """ Terminate user/player session.
    """
    session["player"] = False
    return ok()


if __name__ == "__main__":
    app.secret_key = os.urandom(12)
    app.run(debug=True, host="0.0.0.0", port=4000)
