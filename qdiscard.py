#!/usr/bin/env python3
# coding: utf-8
from __future__ import absolute_import

import click
import functools
import requests
import sys
import time

from PyQt5.QtWidgets import (
    QMainWindow,
    QApplication,
    QWidget,
    QLabel,
    QPushButton,
    QAction,
    QTextEdit,
    QMessageBox,
    QCheckBox,
    QVBoxLayout,
    QDialog,
    QDialogButtonBox,
    QMenu,
)
from PyQt5.QtGui import QPixmap, QImage, QIcon, QPalette
from PyQt5.QtCore import QBuffer, QTimer, Qt


def initialize(s):
    return s[0].upper() + s[1:]


class DiscardClient:
    def __init__(self, discard_server, username, password):
        """ Connect to Discard server.

            @param discard_server (str) Base URL: http://hostname:port
            @param username (str) User name on discard server.
            @param password (str) Password for username.
        """
        self.discard_server = discard_server
        self.session = requests.Session()
        self.session.auth = (username, password)
        resp = self.session.get("{}/login".format(discard_server))
        resp.raise_for_status()
        resp = resp.json()
        self.games = resp["games"]
        self.available_players = resp["players"]
        self.game_types = resp["game_types"]
        self.game = {}

    def new_game(self, game_type, players):
        """ Open up new game of given type, with given players.

            @param game_type (str) Name of the game, e.g. "skat".
            @param players (list of str) List of player usernames.
            @return game_id Game UUID.
        """
        url = "{}/new/{}?".format(self.discard_server, game_type) + "&".join(
            "players={}".format(requests.utils.quote(p)) for p in players
        )
        resp = self.session.get(url)
        resp.raise_for_status()
        game_id = resp.json()["game_id"]
        self.game = {
            "game_id": game_id,
            "game_type": game_type,
            "game_url": "{}/game/{}/{{}}".format(self.discard_server, game_id),
        }
        return game_id

    def join_game(self, game_id):
        """ Join an existing game. Player=user must be in that game.

            @param game_id (str) Game UUID.
        """
        if game_id not in self.games:
            raise ValueError("game does not exist: {}".format(game_id))
        self.game = {
            "game_id": game_id,
            "game_type": self.games[game_id]["game_type"],
            "game_url": "{}/game/{}/{{}}".format(self.discard_server, game_id),
        }
        return game_id

    def leave(self):
        """ Leave current game.
        """
        self.game = {}

    def action(self, command, **kwargs):
        """ Execute an action/command on the current game.

            @param command (str) Game command, e.g. "view", "play", "draw"...
            @param kwargs (dict) Parameters for game command.
            @return Status from discard server.
        """
        if not self.game:
            raise ValueError("no game in play")
        url = self.game["game_url"].format(command)
        resp = self.session.get(url, params=kwargs)
        resp.raise_for_status()
        resp = resp.json()
        if "games" in resp:
            self.games = resp.pop("games", None)
        return resp

    def resource(self, path, **kwargs):
        """ Load resource from discard server.

            @param path (str) Resource path, may contain "{game_id}" and
                "{game_type}" for current game.
            @param kwargs (dict) Parameters for resource URL.
            @return Raw resource data.
        """
        full_path = path.format(
            game_id=self.game.get("game_id", None),
            game_type=self.game.get("game_type", None),
        )
        url = "{}/static/{}".format(self.discard_server, full_path)
        resp = self.session.get(url, params=kwargs)
        resp.raise_for_status()
        return resp.content


class PlayerSelection(QDialog):
    def __init__(self, parent, player, all_players):
        """ Player selection dialog, presenting all players except this player itself.

            `exec_()` this dialog (/!\ it is modal) and if it returns QDialog.Accepted,
            use `players()` method to get list of selected players.

            @param parent (QWidget) parent widget.
            @param player (str) This player.
            @param all_players (list of str) List of all available players.
        """
        super().__init__(parent)
        self.player = player
        layout = QVBoxLayout()
        self._players = []
        self.is_accepted = False
        for player in sorted(all_players):
            if player == self.player:
                continue  # initiating player is always part of the game
            cb = QCheckBox(player)
            cb.setChecked(False)
            self._players.append(cb)
            layout.addWidget(cb)
        button_box = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        button_box.accepted.connect(self.accept)
        button_box.rejected.connect(self.reject)
        layout.addWidget(button_box)
        self.setLayout(layout)
        self.setWindowTitle("Select players")
        self.setModal(True)

    def players(self):
        """ Get list of selected players, including the active player itself.

            @return list of str Selected players.
        """
        result = [self.player]
        for player in self._players:
            if player.isChecked():
                result.append(player.text())
        return result


class CardGame(QMainWindow):

    CARDSIZE = (2 * 37, 2 * 64)
    THUMBNAIL = (30, 45)
    LOG_WIDTH = 200

    def __init__(self, discard, player_name):
        super().__init__()
        self.player_name = player_name
        self.discard = discard  # game server
        self.deck = {}  # card faces cache
        self.view = {}  # current view of the game
        self.resize(1000, 600)
        self.setWindowTitle("QDisCard - {}".format(player_name))
        palette = self.palette()
        palette.setColor(QPalette.Window, Qt.darkGreen)
        self.setPalette(palette)
        self.table = QWidget()
        self.table.show()
        self.setCentralWidget(self.table)
        self.build_menu()
        self.show()
        self.timer = QTimer()
        self.timer.timeout.connect(self.update_view)
        self.timer.start(100)  # milliseconds

    def build_menu(self):
        """ Build menu with game types ("new") and entries for joining
            existing games.
        """
        # FIXME: make this updateable b/c self.discard.games
        main_menu = self.menuBar()
        game_menu = main_menu.addMenu("Game")
        for game_type, description in self.discard.game_types:
            game_entry = QAction("New {} game".format(initialize(game_type)), self)
            game_entry.setStatusTip(description)
            game_entry.triggered.connect(
                functools.partial(self.action_new_game, game_type)
            )
            game_menu.addAction(game_entry)
        game_menu.addSeparator()
        for game in self.discard.games.values():
            game_id = game["game_id"]
            game_type = game["game_type"]
            players = game["players"]
            title = "{} mit {}".format(
                initialize(game_type), ", ".join(initialize(p) for p in sorted(players))
            )
            game_entry = QAction(title, self)
            game_entry.triggered.connect(
                functools.partial(self.action_join_game, game_id=game_id)
            )
            game_menu.addAction(game_entry)
        if self.discard.games:
            game_menu.addSeparator()
        exit_button = QAction("Exit", self)
        exit_button.setShortcut("Ctrl+Q")
        exit_button.setStatusTip("Exit application")
        exit_button.triggered.connect(self.close)
        game_menu.addAction(exit_button)

    def get_card(self, card, type, cardsize=None):
        """ Get card picture.

            Caches resources from discard server.

            @param card (str) Card name (depends on game).
            @param type (...) May be QLabel, QPixmap or "base64".
            @param cardsize (tuple of two int) Optional card size.
        """
        if card not in self.deck:
            print("loading card:", card)
            if card == "background":
                data = self.discard.resource("card_background.png")
            else:
                data = self.discard.resource("{{game_type}}/{}.png".format(card))
            pixmap = QPixmap()
            pixmap.loadFromData(data)
            self.deck[card] = pixmap.scaled(*self.CARDSIZE)
        card = self.deck[card]
        if cardsize:
            card = card.scaled(*cardsize)
        if type == QPixmap:
            return card
        if type == QLabel:
            obj = QLabel(self.table)
            obj.setPixmap(card)
            return obj
        if type == "base64":
            obj = QBuffer()
            obj.open(QBuffer.WriteOnly)
            card.save(obj, "JPG")
            obj.close()
            return "data:image/jpg;base64,{}".format(
                obj.buffer().toBase64().data().decode("utf-8")
            )
        raise TypeError("unknown type {} for card {}".format(type, card))

    def discard_action(self, command, **kwargs):
        """ Execute Discard action and capture error in message box.

            @param command (str) Discard command ("play", "draw", etc).
            @param kwargs (dict) Parameters for discard command.
        """
        try:
            self.discard.action(command, **kwargs)
            print("    discard.action({}, {}) success".format(command, kwargs))
        except Exception as ex:
            print("    discard.action({}, {}) failed: {}".format(command, kwargs, ex))
            error = QMessageBox()
            error.setWindowTitle("QDisCard")
            error.setIcon(QMessageBox.Warning)
            error.setText("Spielzug nicht möglich: {}".format(command))
            error.setDetailedText(
                "{} {}\nError: {}".format(
                    command,
                    ", ".join("{}: {}".format(k, v) for k, v in kwargs.items()),
                    str(ex),
                )
            )
            error.setStandardButtons(QMessageBox.Ok)
            error.exec_()

    def action_join_game(self, game_id):
        self.discard.leave()
        self.discard.join_game(game_id)

    def action_new_game(self, game_type):
        dialog = PlayerSelection(self, self.player_name, self.discard.available_players)
        if dialog.exec_() == QDialog.Accepted:
            players = dialog.players()
            self.discard.leave()
            self.discard.new_game(game_type, players)

    def action_deck(self, mouse_event):
        """ Click on the deck.
        """
        button = mouse_event.button()
        if button == 1:  # left -> take card from deck
            self.discard_action("draw")
        elif button == 2:  # right -> context menu
            ctx = QMenu()
            draw_action = ctx.addAction("Ziehe Karte")
            show_action = ctx.addAction("Decke Karte auf")
            ctx.addSeparator()
            shuffle_action = ctx.addAction("Mischeln")
            action = ctx.exec_(mouse_event.globalPos())
            if action == draw_action:
                self.discard_action("draw")
            elif action == show_action:
                self.discard_action("show")
            elif action == shuffle_action:
                self.discard_action("shuffle")
            else:
                print("action_deck(WTF)")
        else:
            pass

    def action_stack(self, mouse_event, stack):
        """ Click on a stack.
        """
        button = mouse_event.button()
        if button == 1:  # left -> take card from stack
            self.discard_action("draw_from_stack", stack=stack, all=True)
        elif button == 2:  # right -> context menu
            ctx = QMenu()
            draw_action = ctx.addAction("Nimm Karten auf")
            trick_action = ctx.addAction("Nimm Stich auf")
            ctx.addSeparator()
            shuffle_action = ctx.addAction("Karten unter den Stapel")
            action = ctx.exec_(mouse_event.globalPos())
            if action == draw_action:
                self.discard_action("draw_from_stack", stack=stack, all=True)
            elif action == trick_action:
                self.discard_action("take_trick", stack=stack)
            elif action == shuffle_action:
                self.discard_action("stack_to_deck", stack=stack)
            else:
                print("action_stack(WTF)")
        else:
            pass

    def action_hand(self, mouse_event, card):
        """ Click on a card in the hand.
        """
        button = mouse_event.button()
        if button == 1:  # left -> play card
            self.discard_action("play", card=card)
        elif button == 2:  # right -> context menu
            ctx = QMenu()
            play_action = ctx.addAction("Spiele Karte")
            push_action = ctx.addAction("Drücke Karte")
            action = ctx.exec_(mouse_event.globalPos())
            if action == play_action:
                self.discard_action("play", card=card)
            elif action == push_action:
                self.discard_action("push", card=card)
            else:
                print("action_hand(WTF)")
        else:
            pass

    def update_view(self):
        """ Check if view needs to be updated.

            This is triggered periodically, gets the current view of the game
            from the discard server, and updates the view if new log entries
            are present.
        """
        if self.discard.game:
            new_view = self.discard.action("view")["view"]
            if not self.view or self.view["log"][-1] != new_view["log"][-1]:
                self.view = new_view
                self.show_game()

    def show_game(self):
        """ Show game: player's hand, opponents, stacks and deck. Oh, and the
            log so you can see what the others did.
        """
        if not self.discard.game:
            return  # no game yet
        # clear old game view first
        for child in list(self.table.children()):
            child.setParent(None)
        self.show_hand(self.view)
        self.show_deck_and_stacks(self.view)
        self.show_opponents(self.view)
        self.show_log(self.view)
        self.table.show()

    def show_hand(self, view):
        """ Show player's hand.

            @param view (dict) Player's view of the game.
        """
        for i, card in enumerate(view["hand"]):
            pos = (
                10 + i * (10 + self.CARDSIZE[0]),
                self.table.height() - self.CARDSIZE[1] - 10,
            )
            c = self.get_card(card, type=QLabel)
            c.mousePressEvent = functools.partial(self.action_hand, card=card)
            c.move(*pos)
            c.show()

    def show_deck_and_stacks(self, view):
        """ Show deck and stacks.

            @param view (dict) Player's view of the game.
        """
        # two stages: collect columns (stacks and deck) so we know how to space
        # them, then actually draw them.
        columns = []
        for stack_name, stack in view["stack"].items():
            for card in stack:
                c = self.get_card(card, type=QLabel)
                c.mousePressEvent = functools.partial(
                    self.action_stack, stack=stack_name
                )
                columns.append(c)
        if view["deck"]:
            c = self.get_card("background", type=QLabel)
            c.mousePressEvent = self.action_deck
            columns.append(c)
        # actually position and draw them
        offset = (
            self.table.width()
            - self.LOG_WIDTH
            - 20
            - len(columns) * (self.CARDSIZE[0] + 10)
        ) // 2
        for i, card in enumerate(columns):
            pos = (
                offset + i * (self.CARDSIZE[0] + 10),
                (self.table.height() - self.CARDSIZE[1]) // 2,
            )
            card.move(*pos)
            card.show()

    def show_opponents(self, view):
        """ Show names and cards (well, backsides anyway) of the opponents.

            @param view (dict) Player's view of the game.
        """
        players = view["players"]
        player_width = (
            self.table.width() - self.LOG_WIDTH - 10 * (len(players) + 1)
        ) // len(players)
        for i, (player, count) in enumerate(view["players"].items()):
            pos = 10 + i * player_width, 10
            print("player {} ({} cards) at {}".format(player, count, pos))
            player_label = QLabel(initialize(player), self.table)
            player_label.move(*pos)
            player_label.show()
            for j in range(count):
                card = self.get_card("background", type=QLabel)
                card.move(pos[0] + j * 10, pos[1] + player_label.height() + 5)
                card.show()

    def show_log(self, view):
        """ Show what has happened in the game.

            @param view (dict) Player's view of the game.
        """

        def img_thumbnail(card):
            return '<img alt="{}" src="{}" />'.format(
                card,
                self.get_card(
                    card or "background", type="base64", cardsize=self.THUMBNAIL
                ),
            )

        def format_log(ts, player, action, kwargs):
            ts = time.localtime(ts)
            when = "{:2}:{:02}:{:02}".format(ts.tm_hour, ts.tm_min, ts.tm_sec)
            if action == "shuffle":
                what = "mischeln"
                objects = ""
            elif action == "stack_to_deck":
                what = "Stapel {} wiederverwenden".format(kwargs["stack"] or "")
                objects = "({} Karten)".format(kwargs["n"])
            elif action == "deal":
                what = "austeilen"
                objects = "({} Karten)".format(kwargs["n"])
            elif action == "play":
                what = "spielt"
                objects = ", ".join(img_thumbnail(c) for c in kwargs["cards"])
            elif action == "draw":
                what = "zieht"
                objects = ", ".join(img_thumbnail(c) for c in kwargs["cards"])
            elif action == "push":
                what = "drückt"
                objects = ", ".join(img_thumbnail(c) for c in kwargs["cards"])
            elif action == "show":
                what = "deckt auf"
                objects = ", ".join(img_thumbnail(c) for c in kwargs["cards"])
            else:
                what = action
                objects = ", ".join("{}: {}".format(k, v) for k, v in kwargs.items())
            return "{when} <b>{who}</b> <i>{what}</i><br /> {objects}".format(
                when=when,
                who=initialize(player) if player else "",
                what=what,
                objects=objects,
            )

        text = "\n".join(
            "<p> {} </p>".format(format_log(*entry)) for entry in reversed(view["log"])
        )
        log = QTextEdit(text, self.table)
        log.setGeometry(
            self.table.width() - self.LOG_WIDTH - 10,
            10,
            self.LOG_WIDTH,
            self.table.height() - 20,
        )
        log.setReadOnly(True)
        log.show()


@click.command()
@click.option("--discard-server", default="http://localhost:4000")
@click.option("--user", default="riker")
@click.option("--password", default="number1")
def cli(discard_server, user, password):
    discard = DiscardClient(discard_server, user, password)
    app = QApplication(sys.argv)
    card_game = CardGame(discard, user)
    card_game.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    cli()
