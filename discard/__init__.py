#!/usr/bin/env python3
# coding: utf-8
from __future__ import absolute_import


import copy
import json
import random
import time


class Game:
    def __init__(
        self,
        game_type,
        deck,
        players,
        stacks=[None],
        stack_visible=1,
        tricks=False,
        auto_sort=False,
    ):
        """ Prepare a card game.

            @param game_type (str) Type (name) of the game.
            @param deck (list) List of available cards.
            @param players (list of str) The players.
            @param stacks (list of str) List of stacks (names).
            @param stack_visible (int) How much of the stack is visible (top n cards).
            @param tricks (bool) Game allows for tricks.
            @param auto_sort (bool) Automatically sort player cards.
        """
        self.game_type = game_type
        self.auto_sort = auto_sort
        self.stack_visible = stack_visible
        self.deck = copy.deepcopy(deck)
        self.hand = {player: [] for player in players}
        if stacks:
            self.stack = {stack: [] for stack in stacks}
        else:
            self.stack = {}
        if tricks:
            self.trick = {player: [] for player in players}
        else:
            self.trick = None
        self.log = []

    def __str__(self):
        return json.dumps(
            {
                "game_type": self.game_type,
                "auto_sort": self.auto_sort,
                "stack_visible": self.stack_visible,
                "deck": self.deck,
                "hand": self.hand,
                "stack": self.stack,
                "trick": self.trick,
                "log": self.log,
            },
            indent=4,
            sort_keys=True,
        )

    __repr__ = __str__

    def get_log(self, player):
        public_actions = [
            "shuffle",
            "stack_to_deck",
            "deal",
            "show",
            "play",
            "draw_from_stack",
            "take_trick",
        ]

        def _sanitize(kwargs, visible):
            if visible:
                return kwargs
            kwargs = copy.deepcopy(kwargs)
            kwargs["cards"] = [None] * len(kwargs["cards"])
            return kwargs

        log = [
            (
                ts,
                log_player,
                action,
                _sanitize(kwargs, log_player == player or action in public_actions),
            )
            for ts, log_player, action, kwargs in self.log
        ]
        return log

    def view(self, player):
        result = {
            "player": player,
            "hand": self.hand[player],
            "deck": bool(self.deck),
            "log": self.get_log(player),
        }
        if self.trick:
            result["trick"] = self.trick[player]
        else:
            result["trick"] = None
        result["players"] = {
            p: len(cards) for p, cards in self.hand.items() if p != player
        }
        result["stack"] = {
            stack_name: stack[-self.stack_visible :] if self.stack_visible else []
            for stack_name, stack in self.stack.items()
        }
        return result

    def shuffle(self):
        """ Shuffle the deck.
        """
        random.shuffle(self.deck)
        self.log.append((time.time(), None, "shuffle", {}))

    def stack_to_deck(self, stack=None):
        """ Replace all cards from stack to the deck, and reshuffle.
        """
        self.deck.extend(self.stack[stack])
        random.shuffle(self.deck)
        self.log.append(
                (time.time(), None, "stack_to_deck", {"n": len(self.stack[stack]), "stack": stack})
        )
        self.stack[stack] = []

    def deal(self, n):
        """ Deal n cards to each player.
        """
        for _ in range(n):
            for player in self.hand.keys():
                self.hand[player].append(self.deck.pop())
        if self.auto_sort:
            for player in self.hand.keys():
                self.hand[player] = sorted(self.hand[player])
        self.log.append((time.time(), None, "deal", {"n": n}))

    def draw(self, player):
        """ Player draws one card from the deck.
        """
        card = self.deck.pop()
        self.hand[player].append(card)
        if self.auto_sort:
            self.hand[player] = sorted(self.hand[player])
        _, last_player, last_action, last_kwargs = self.log[-1]
        if last_player == player and last_action == "draw":
            last_kwargs["cards"].append(card)
        else:
            self.log.append((time.time(), player, "draw", {"cards": [card]}))

    def show(self, stack=None):
        """ Take one cards from the deck and show it on the stack.
        """
        card = self.deck.pop()
        self.stack[stack].append(card)
        self.log.append((time.time(), None, "show", {"cards": [card], "stack": stack}))

    def draw_from_stack(self, player, stack=None, all=False):
        """ Player takes top card (or all cards) from stack.
        """
        if all:
            cards = self.stack[stack]
            self.stack[stack] = []
        else:
            cards = [self.stack[stack].pop()]
        self.hand[player].extend(cards)
        if self.auto_sort:
            self.hand[player] = sorted(self.hand[player])
        self.log.append(
            (time.time(), player, "draw_from_stack", {"cards": cards, "stack": stack})
        )

    def push(self, player, card):
        """ Player pushes card to their tricks.
        """
        self.hand[player].remove(card)
        if self.auto_sort:
            self.hand[player] = sorted(self.hand[player])
        self.trick[player].append(card)
        self.log.append((time.time(), player, "push", {"cards": [card]}))

    def play(self, player, card, stack=None):
        """ Player places card on the stack.
        """
        self.hand[player].remove(card)
        if self.auto_sort:
            self.hand[player] = sorted(self.hand[player])
        self.stack[stack].append(card)
        self.log.append(
            (time.time(), player, "play", {"cards": [card], "stack": stack})
        )

    def take_trick(self, player, stack=None):
        """ Player takes the trick (all cards on the stack).
        """
        trick = self.stack[stack]
        self.stack[stack] = []
        self.trick[player].extend(trick)
        self.log.append(
            (time.time(), player, "take_trick", {"cards": trick, "stack": stack})
        )
