# discard

DIStributed CARD game.

Play card games over the internet.

This is more of a framework for mapping real world card games to be playable
remotely.


## Running discard

For some reason, pip install pyqt5 messes up the PyQt installation so the
client won't work. You need to use distribution packages, e.g. for Debian:

    sudo apt install python3-pyqt5 python3-click python3-requests python3-flask

### Installing a Game

Place the cards pics in `./static/<gamename>/`, for example:

    tar xvf skat.tar

and declare them in `./games.json`. This is a JSON dict with the game name as
key, and a game dict as value. The game dict looks like this:

    {
        "description": "Skat",    # name for the menu
        "auto_sort": true,        # sort cards
        "deal": {"default": 10},  # deal 10 cards to each player
        "tricks": true,           # game knowns tricks
        "deck": ["Kreuz Bube", "Kreuz As", "Kreuz 10", ...],
    }
